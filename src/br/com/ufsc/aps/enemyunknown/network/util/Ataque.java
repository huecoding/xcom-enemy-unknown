/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ufsc.aps.enemyunknown.network.util;

import br.com.ufsc.aps.enemyunknown.model.Posicao;

/**
 *
 * @author raphael
 */
public class Ataque extends JogadaEU {

    private final int ataqueMaximo;
    private final int ataqueMinimo;

    public Ataque(int ataqueMaximo, int ataqueMinimo, Posicao origem, Posicao destino) {
        super(origem, destino);
        this.ataqueMaximo = ataqueMaximo;
        this.ataqueMinimo = ataqueMinimo;
    }

    public int getAtaqueMaximo() {
        return ataqueMaximo;
    }

    public int getAtaqueMinimo() {
        return ataqueMinimo;
    }

}

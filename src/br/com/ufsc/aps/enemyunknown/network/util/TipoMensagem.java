/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ufsc.aps.enemyunknown.network.util;

/**
 *
 * @author raphael
 */
public enum TipoMensagem {

	INICIA_PARTIDA,
	CANCELAR_INICIO_PARTIDA,
	TROPA_ESCOLHIDA,
	PRONTO,
	PASSOU_A_VEZ,
	ABANDONAR_PARTIDA

}

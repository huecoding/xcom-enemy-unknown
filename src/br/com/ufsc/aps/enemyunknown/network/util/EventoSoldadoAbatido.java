/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ufsc.aps.enemyunknown.network.util;

import br.com.ufsc.aps.enemyunknown.model.Posicao;

/**
 *
 * @author Raphael
 */
public class EventoSoldadoAbatido extends EventoRede {

	private Posicao posicaoSoldado;

	public EventoSoldadoAbatido(Posicao posicaoSoldado) {
		this.posicaoSoldado = posicaoSoldado;
	}

	public Posicao getPosicaoSoldado() {
		return posicaoSoldado;
	}

	public void setPosicaoSoldado(Posicao posicaoSoldado) {
		this.posicaoSoldado = posicaoSoldado;
	}

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ufsc.aps.enemyunknown.network.util;

import br.com.ufsc.aps.enemyunknown.model.Jogador;
import br.ufsc.inf.leobr.cliente.Jogada;

/**
 *
 * @author raphael
 */
public class Mensagem implements Jogada {

    private final String nomeJogador;
    private TipoMensagem tipo;

    public Mensagem(Jogador jogador, TipoMensagem tipo) {
        this.nomeJogador = jogador.getNome();
        this.tipo = tipo;
    }

    public String getNomeJogador() {
        return nomeJogador;
    }
    
    public TipoMensagem getTipo() {
        return tipo;
    }

    public void setTipo(TipoMensagem tipo) {
        this.tipo = tipo;
    }

}

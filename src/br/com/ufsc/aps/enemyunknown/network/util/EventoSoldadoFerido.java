/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ufsc.aps.enemyunknown.network.util;

/**
 *
 * @author Raphael
 */
public class EventoSoldadoFerido extends EventoRede {

	private int danoCausado;

	public EventoSoldadoFerido(int danoCausado) {
		this.danoCausado = danoCausado;
	}

	public int getDanoCausado() {
		return danoCausado;
	}

	public void setDanoCausado(int danoCausado) {
		this.danoCausado = danoCausado;
	}

}

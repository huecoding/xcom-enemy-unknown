/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ufsc.aps.enemyunknown.network.util;

import br.com.ufsc.aps.enemyunknown.model.Posicao;

/**
 *
 * @author raphael
 */
public abstract class JogadaEU extends EventoRede {

	private Posicao origem;
	private Posicao destino;

	public JogadaEU(Posicao origem, Posicao destino) {
		this.origem = origem;
		this.destino = destino;
	}

	public Posicao getOrigem() {
		return origem;
	}

	public void setOrigem(Posicao origem) {
		this.origem = origem;
	}

	public Posicao getDestino() {
		return destino;
	}

	public void setDestino(Posicao destino) {
		this.destino = destino;
	}

}

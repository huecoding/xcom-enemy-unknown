/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ufsc.aps.enemyunknown.network.util;

import br.com.ufsc.aps.enemyunknown.model.Batalha;

/**
 *
 * @author Raphael
 */
public class EventoFimBatalha extends EventoRede {

	private Batalha batalha;

	public EventoFimBatalha(Batalha batalha) {
		this.batalha = batalha;
	}

	public Batalha getBatalha() {
		return batalha;
	}

	public void setBatalha(Batalha batalha) {
		this.batalha = batalha;
	}

}

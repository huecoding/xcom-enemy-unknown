/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ufsc.aps.enemyunknown.network;

import br.com.ufsc.aps.enemyunknown.EnemyUnknown;
import br.com.ufsc.aps.enemyunknown.exception.FaltamJogadoresException;
import br.com.ufsc.aps.enemyunknown.exception.NomeJaExisteNoServidorException;
import br.com.ufsc.aps.enemyunknown.exception.ServidorCheioException;
import br.com.ufsc.aps.enemyunknown.model.Batalha;
import br.com.ufsc.aps.enemyunknown.model.JogadorPessoa;
import br.com.ufsc.aps.enemyunknown.model.Posicao;
import br.com.ufsc.aps.enemyunknown.network.util.EventoFimBatalha;
import br.com.ufsc.aps.enemyunknown.network.util.EventoRede;
import br.com.ufsc.aps.enemyunknown.network.util.EventoSoldadoAbatido;
import br.com.ufsc.aps.enemyunknown.network.util.EventoSoldadoFerido;
import br.com.ufsc.aps.enemyunknown.network.util.JogadaEU;
import br.com.ufsc.aps.enemyunknown.network.util.Mensagem;
import br.com.ufsc.aps.enemyunknown.network.util.OuvidorProxyAdapter;
import br.com.ufsc.aps.enemyunknown.network.util.TipoMensagem;
import br.com.ufsc.aps.enemyunknown.util.AtorJogador;
import br.com.ufsc.aps.enemyunknown.view.CampoBatalha;
import br.ufsc.inf.leobr.cliente.Jogada;
import br.ufsc.inf.leobr.cliente.Proxy;
import br.ufsc.inf.leobr.cliente.exception.NaoConectadoException;
import br.ufsc.inf.leobr.cliente.exception.NaoJogandoException;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;

/**
 *
 * @author raphael
 */
public class AtorNetGames extends OuvidorProxyAdapter {

	private static final AtorNetGames instance = new AtorNetGames();

	public static AtorNetGames getInstance() {
		return instance;
	}

	private final Proxy proxy;
	private JogadorPessoa jogadorAtual;
	private String endereco;

	public AtorNetGames() {
		proxy = Proxy.getInstance();
		proxy.addOuvinte(this);
		jogadorAtual = new JogadorPessoa();
	}

	public void iniciarPartida() throws NaoConectadoException, FaltamJogadoresException {
		boolean temJogadoresSuficiente = CollectionUtils.isNotEmpty(proxy.obterNomeAdversarios());
		if (temJogadoresSuficiente) {
			proxy.iniciarPartida(2);
			jogadorAtual.setOrdem(1);
		} else {
			throw new FaltamJogadoresException();
		}
	}

	@Override
	public void iniciarNovaPartida(Integer posicao) {
		if (posicao == 1) {
			AtorJogador.setAguardando(true);
			AtorJogador.setBloqueioBotoes(true);
		} else {
			jogadorAtual.setOrdem(2);
			String adversario = proxy.obterNomeAdversario(1);
			boolean aceitouInicio = AtorJogador.notificarSolicitacaoInicio(adversario);
			if (aceitouInicio) {
				enviarMensagem(TipoMensagem.INICIA_PARTIDA);
				AtorJogador.escolherTropa();
			} else {
				enviarMensagem(TipoMensagem.CANCELAR_INICIO_PARTIDA);
			}
		}
	}

	private void testaNomeValido(String nome) throws NomeJaExisteNoServidorException {
		List<String> adversarios = proxy.obterNomeAdversarios();
		if (CollectionUtils.isNotEmpty(adversarios)) {
			for (String adversario : adversarios) {
				if (adversario.equalsIgnoreCase(nome)) {
					throw new NomeJaExisteNoServidorException();
				}
			}
		}
	}

	private void testaServidorCheio() throws ServidorCheioException {
		if (CollectionUtils.size(proxy.obterNomeAdversarios()) > 2) {
			throw new ServidorCheioException();
		}
	}

	public void tentaConectar(String endereco) throws Exception {
		String nomeJogador = jogadorAtual.getNome();
		proxy.conectar(endereco, nomeJogador);
		try {
			testaNomeValido(nomeJogador);
			testaServidorCheio();
			this.endereco = endereco;
		} catch (NomeJaExisteNoServidorException | ServidorCheioException e) {
			desconectar();
			throw e;
		}
	}

	public void enviarMensagem(TipoMensagem tipoMensagem) {
		try {
			proxy.enviaJogada(new Mensagem(jogadorAtual, tipoMensagem));
		} catch (NaoJogandoException ex) {
			AtorJogador.exibirFalhaGenerica(ex);
		}
	}

	public void enviarJogada(JogadaEU jogada) {
		try {
			proxy.enviaJogada(jogada);
		} catch (NaoJogandoException ex) {
			AtorJogador.exibirFalhaGenerica(ex);
		}
	}

	public void enviarEventoRede(EventoRede eventoRede) {
		try {
			proxy.enviaJogada(eventoRede);
		} catch (NaoJogandoException ex) {
			AtorJogador.exibirFalhaGenerica(ex);
		}
	}

	@Override
	public void receberJogada(Jogada jogada) {
		if (jogada instanceof Mensagem) {
			receberMensagem((Mensagem) jogada);
		} else if (jogada instanceof JogadaEU) {
			receberJogadaEU((JogadaEU) jogada);
		} else if (jogada instanceof EventoRede) {
			receberEventoRede((EventoRede) jogada);
		}
	}

	private void receberEventoRede(EventoRede eventoRede) {
		if (eventoRede instanceof EventoSoldadoFerido) {
			EventoSoldadoFerido esf = (EventoSoldadoFerido) eventoRede;
			AtorJogador.exibirDanoCausado(esf.getDanoCausado());
		} else if (eventoRede instanceof EventoSoldadoAbatido) {
			EventoSoldadoAbatido esa = (EventoSoldadoAbatido) eventoRede;
			AtorJogador.exibirSoldadoAbatido();
			CampoBatalha.getInstance().removerSoldadoAbatido(espelharPosicao(esa.getPosicaoSoldado()));
		} else if (eventoRede instanceof EventoFimBatalha) {
			EventoFimBatalha efb = (EventoFimBatalha) eventoRede;
			AtorJogador.exibirResumo(efb.getBatalha());
			CampoBatalha.tratarFimBatalha();
			EnemyUnknown.getTelaInicial().setVisible(true);
		}
	}

	private void receberJogadaEU(JogadaEU jogada) {
		jogada.setOrigem(espelharPosicao(jogada.getOrigem()));
		jogada.setDestino(espelharPosicao(jogada.getDestino()));
		CampoBatalha.getInstance().tratarJogada(jogada);
	}

	private Posicao espelharPosicao(Posicao posicao) {
		return new Posicao(
			posicao.getX(),
			29 - posicao.getY()
		);
	}

	private void receberMensagem(Mensagem mensagem) {
		TipoMensagem tipoMensagem = mensagem.getTipo();
		switch (tipoMensagem) {
			case INICIA_PARTIDA:
				AtorJogador.setAguardando(false);
				AtorJogador.escolherTropa();
				AtorJogador.setBloqueioBotoes(false);
				break;

			case CANCELAR_INICIO_PARTIDA:
				try {
					AtorJogador.exibirJogadorRecusouInicio();
					AtorJogador.setBloqueioBotoes(false);
					AtorJogador.setAguardando(false);
					proxy.finalizarPartida();
				} catch (NaoConectadoException | NaoJogandoException ex) {
					AtorJogador.exibirFalhaGenerica(ex);
				}
				break;

			case TROPA_ESCOLHIDA:
				if (jogadorAtual.getTropa() != null) {
					AtorJogador.setAguardando(false);
					enviarMensagem(TipoMensagem.PRONTO);
					AtorJogador.exibirCampoDeBatalha(new Batalha());
				}
				break;

			case PRONTO:
				AtorJogador.setAguardando(false);
				AtorJogador.exibirCampoDeBatalha(new Batalha());
				break;

			case PASSOU_A_VEZ:
				CampoBatalha.getInstance().tratarPassarVez();
				break;

			case ABANDONAR_PARTIDA:
				AtorJogador.exibirJogadorAbandonouPartida(mensagem.getNomeJogador());
				CampoBatalha.tratarFimBatalha();
				EnemyUnknown.getTelaInicial().setVisible(true);
		}
	}

	public void desconectar() throws NaoConectadoException {
		proxy.desconectar();
		endereco = null;
		jogadorAtual.setTropa(null);
		jogadorAtual.setOrdem(0);
	}

	public JogadorPessoa getJogadorAtual() {
		return jogadorAtual;
	}

	public String getEndereco() {
		return endereco;
	}

}

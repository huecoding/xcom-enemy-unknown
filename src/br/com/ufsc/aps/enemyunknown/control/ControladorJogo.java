/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ufsc.aps.enemyunknown.control;

import br.com.ufsc.aps.enemyunknown.EnemyUnknown;
import br.com.ufsc.aps.enemyunknown.model.Batalha;
import br.com.ufsc.aps.enemyunknown.model.JogadorPessoa;
import br.com.ufsc.aps.enemyunknown.model.Posicao;
import br.com.ufsc.aps.enemyunknown.model.Soldado;
import br.com.ufsc.aps.enemyunknown.network.AtorNetGames;
import br.com.ufsc.aps.enemyunknown.network.util.Ataque;
import br.com.ufsc.aps.enemyunknown.network.util.EventoFimBatalha;
import br.com.ufsc.aps.enemyunknown.network.util.EventoSoldadoAbatido;
import br.com.ufsc.aps.enemyunknown.network.util.EventoSoldadoFerido;
import br.com.ufsc.aps.enemyunknown.network.util.Movimento;
import br.com.ufsc.aps.enemyunknown.network.util.TipoMensagem;
import br.com.ufsc.aps.enemyunknown.util.AtorJogador;
import br.com.ufsc.aps.enemyunknown.view.CampoBatalha;

/**
 *
 * @author Raphael
 */
public class ControladorJogo {
	
	private static ControladorJogo instance = new ControladorJogo();

	public static ControladorJogo getInstance() {
		return instance;
	}
	
	private int vez = 1;
	
	public JogadorPessoa getJogadorAtual() {
		return AtorNetGames.getInstance().getJogadorAtual();
	}
	
	public void enviaAtaque(Soldado soldado, Posicao origem, Posicao destino) {
		AtorNetGames.getInstance().enviarJogada(new Ataque(soldado.getAtaqueMaximo(), soldado.getAtaqueMinimo(), origem, destino));
	}
	
	public void enviaMovimento(Posicao origem, Posicao destino) {
		AtorNetGames.getInstance().enviarJogada(new Movimento(origem, destino));
	}
	
	public void verificaPassaVez() {
		if (!CampoBatalha.getInstance().isTemBotaoHabilitado()) {
			passarVez();
			AtorNetGames.getInstance().enviarMensagem(TipoMensagem.PASSOU_A_VEZ);
		}
	}
	
	public void passarVez() {
		if (vez == 1) {
			vez = 2;
		} else {
			vez = 1;
		}
		CampoBatalha.getInstance().atualizaLabelVez();
	}
	
	public boolean isVezDoJogador() {
		return vez == AtorNetGames.getInstance().getJogadorAtual().getOrdem();
	}
	
	public void enviarEventoSoldadoFerido(int dano) {
		AtorNetGames.getInstance().enviarEventoRede(new EventoSoldadoFerido(dano));
	}
	
	public void enviarEventoSoldadoAbatido(Posicao posicaoSoldado) {
		AtorNetGames.getInstance().enviarEventoRede(new EventoSoldadoAbatido(posicaoSoldado));
	}
	
	public void enviarEventoFimBatalha(Batalha batalha) {
		AtorNetGames.getInstance().enviarEventoRede(new EventoFimBatalha(batalha));
	}

	public void verificaDerrota() {
		boolean aliadosMortos = CampoBatalha.getInstance().isTodosSoldadosMortos();
		if (aliadosMortos) {
			Batalha batalha = CampoBatalha.getInstance().getBatalha();
			batalha.finalizar(vez);
			enviarEventoFimBatalha(batalha);
			AtorJogador.exibirResumo(batalha);
			CampoBatalha.tratarFimBatalha();
			EnemyUnknown.getTelaInicial().setVisible(true);
		}
	}
	
}

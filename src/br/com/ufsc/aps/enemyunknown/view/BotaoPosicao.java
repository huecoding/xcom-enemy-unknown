/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ufsc.aps.enemyunknown.view;

import br.com.ufsc.aps.enemyunknown.control.ControladorJogo;
import br.com.ufsc.aps.enemyunknown.model.Posicao;
import br.com.ufsc.aps.enemyunknown.model.Soldado;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JToggleButton;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author raphael
 */
public final class BotaoPosicao extends JToggleButton {

	private static final Font FONTE = new Font("Tahoma", Font.PLAIN, 8);

	private static final Color COR_LETRA = Color.WHITE;

	private static final Color COR_PADRAO = null;
	private static final Color COR_AMIGO = Color.BLUE;
	private static final Color COR_INIMIGO = Color.RED;
	private static final Color COR_ALCANCE = Color.YELLOW;

	private final Posicao posicao;
	private Soldado soldadoOcupante;
	private boolean aindaNaoInteragiu;

	public BotaoPosicao(Posicao posicao) {
		super();
		this.posicao = posicao;
		this.soldadoOcupante = null;
		aindaNaoInteragiu = true;
		setEnabled(false);
		setFont(FONTE);
		setForeground(COR_LETRA);
	}

	@Override
	public void setEnabled(boolean b) {
		super.setEnabled(b && ControladorJogo.getInstance().isVezDoJogador());
	}

	public void exibirComoAlcance(boolean exibir) {
		if (soldadoOcupante != null) {
			setBackground(soldadoOcupante.isAmigo() ? COR_AMIGO : COR_INIMIGO);
			setEnabled((exibir && aindaNaoInteragiu) || (aindaNaoInteragiu && soldadoOcupante.isAmigo()));
		} else if (exibir) {
			setBackground(COR_ALCANCE);
			setEnabled(true);
		} else {
			setBackground(null);
			setEnabled(false);
		}
	}

	public boolean isAlcance() {
		return isEnabled() && soldadoOcupante == null;
	}

	public void posicionarSoldado(Soldado soldado) {
		soldadoOcupante = soldado;
		if (soldado != null) {
			setText(String.valueOf(soldadoOcupante.getOrdem() > 0 ? soldadoOcupante.getOrdem() : StringUtils.EMPTY));
			setBackground(soldadoOcupante.isAmigo() ? COR_AMIGO : COR_INIMIGO);
			setEnabled(soldadoOcupante.isAmigo());
		} else {
			setEnabled(false);
			setText(null);
			setBackground(COR_PADRAO);
		}
	}

	public Soldado removerSoldado() {
		Soldado s = soldadoOcupante;
		posicionarSoldado(null);
		return s;
	}

	public Posicao getPosicao() {
		return posicao;
	}

	public Soldado getSoldadoOcupante() {
		return soldadoOcupante;
	}

	public boolean isAindaNaoInteragiu() {
		return aindaNaoInteragiu;
	}

	public void setAindaNaoInteragiu(boolean aindaNaoInteragiu) {
		this.aindaNaoInteragiu = aindaNaoInteragiu;
	}

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ufsc.aps.enemyunknown.view;

import br.com.ufsc.aps.enemyunknown.control.ControladorJogo;
import br.com.ufsc.aps.enemyunknown.model.Batalha;
import br.com.ufsc.aps.enemyunknown.model.JogadorPessoa;
import br.com.ufsc.aps.enemyunknown.model.Posicao;
import br.com.ufsc.aps.enemyunknown.model.Soldado;
import br.com.ufsc.aps.enemyunknown.model.SoldadoInimigo;
import br.com.ufsc.aps.enemyunknown.model.Tropa;
import br.com.ufsc.aps.enemyunknown.network.util.Ataque;
import br.com.ufsc.aps.enemyunknown.network.util.JogadaEU;
import br.com.ufsc.aps.enemyunknown.network.util.Movimento;
import br.com.ufsc.aps.enemyunknown.util.AtorJogador;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.JLabel;
import org.apache.commons.collections4.CollectionUtils;

/**
 *
 * @author Raphael
 */
public class CampoBatalha extends EnemyUnknownBaseFrame {

	private static CampoBatalha instance = new CampoBatalha();

	public static CampoBatalha getInstance() {
		return instance;
	}

	public static void tratarFimBatalha() {
		instance.dispose();
		instance = new CampoBatalha();
	}

	private static final List<Posicao> POSICOES_AMIGOS = Arrays.asList(
		new Posicao(15, 22),
		new Posicao(5, 24),
		new Posicao(25, 24),
		new Posicao(11, 27),
		new Posicao(19, 27)
	);

	private static final List<Posicao> POSICOES_INIMIGOS = Arrays.asList(
		new Posicao(11, 2),
		new Posicao(19, 2),
		new Posicao(5, 5),
		new Posicao(25, 5),
		new Posicao(15, 7)
	);

	private static final int TAMANHO_CAMPO_BATALHA = 31;

	private static final Color COR_VIDA_ALTA = Color.GREEN;
	private static final Color COR_VIDA_REGULAR = Color.YELLOW;
	private static final Color COR_VIDA_BAIXA = Color.RED;

	private Batalha batalha;
	private Tropa tropaAliada;

	private BotaoPosicao btnPrimeiroClique;
	private final List<Integer> indicesAlcances = new ArrayList<>();

	public CampoBatalha() {
		super("Campo de batalha");
		initComponents();
		setExtendedState(MAXIMIZED_BOTH);
		for (int y = 0; y < TAMANHO_CAMPO_BATALHA; y++) {
			for (int x = 0; x < TAMANHO_CAMPO_BATALHA; x++) {
				BotaoPosicao btn = new BotaoPosicao(new Posicao(x, y));
				btn.addActionListener((ActionEvent e) -> {
					AtorJogador.trataCliqueCampo(btn);
				});
				painelCampoBatalha.add(btn);
			}
		}
	}

	public void iniciarBatalha(Batalha batalha) {
		JogadorPessoa j = ControladorJogo.getInstance().getJogadorAtual();
		lbOrdemJogador.setText(String.valueOf(j.getOrdem()));
		tropaAliada = j.getTropa();
		posicionarTropas();
		setVisible(true);
		this.batalha = batalha;
		batalha.iniciar();
		atualizarResumoTropa();
		atualizaLabelVez();
	}

	public void atualizaLabelVez() {
		if (ControladorJogo.getInstance().isVezDoJogador()) {
			lbVez.setText("Sua vez!");
			lbVez.setForeground(Color.GREEN);
		} else {
			lbVez.setText("Aguarde o adversário...");
			lbVez.setForeground(Color.RED);
		}
	}

	public boolean isTemBotaoHabilitado() {
		BotaoPosicao b;
		for (Component c : painelCampoBatalha.getComponents()) {
			b = (BotaoPosicao) c;
			if (b.isEnabled()) {
				return true;
			}
		}
		return false;
	}

	public void tratarPassarVez() {
		ControladorJogo.getInstance().passarVez();
		if (ControladorJogo.getInstance().isVezDoJogador()) {
			BotaoPosicao btn;
			for (Component comp : painelCampoBatalha.getComponents()) {
				btn = (BotaoPosicao) comp;
				Soldado soldadoOcupante = btn.getSoldadoOcupante();
				if (soldadoOcupante != null) {
					btn.setAindaNaoInteragiu(true);
					btn.setEnabled(soldadoOcupante.isAmigo());
				}
			}
		}
	}

	private void posicionarTropas() {
		BotaoPosicao btn;
		int soldado = 0;

		List<Soldado> soldadosAliados = tropaAliada.getSoldados();
		List<Posicao> posicoesAmigos = new ArrayList<>(POSICOES_AMIGOS);
		List<Posicao> posicoesInimigos = new ArrayList<>(POSICOES_INIMIGOS);
		Posicao posAmigo = posicoesAmigos.get(0);
		Posicao posInimigo = posicoesInimigos.get(0);

		for (Component comp : painelCampoBatalha.getComponents()) {
			if (comp instanceof BotaoPosicao) {
				btn = (BotaoPosicao) comp;
				if (posAmigo != null && posAmigo.equals(btn.getPosicao())) {
					btn.posicionarSoldado(soldadosAliados.get(soldado++));
					posicoesAmigos.remove(0);
					if (posicoesAmigos.size() > 0) {
						posAmigo = posicoesAmigos.get(0);
					} else {
						posAmigo = null;
					}
				} else if (posInimigo != null && posInimigo.equals(btn.getPosicao())) {
					btn.posicionarSoldado(new SoldadoInimigo());
					posicoesInimigos.remove(0);
					if (posicoesInimigos.size() > 0) {
						posInimigo = posicoesInimigos.get(0);
					} else {
						posInimigo = null;
					}
				}

				if (CollectionUtils.isEmpty(posicoesAmigos) && CollectionUtils.isEmpty(posicoesInimigos)) {
					return;
				}
			}
		}
	}

	public void cliqueCampo(BotaoPosicao btn) {
		Soldado s = btn.getSoldadoOcupante();
		boolean temSoldado = s != null;
		boolean isAmigo = temSoldado && s.isAmigo();
		boolean isAindaNaoInteragiu = btn.isAindaNaoInteragiu();
		
		if (btnPrimeiroClique == null) {
			// Primeiro clique
			if (temSoldado) {
				exibeDadosSoldado(s);
				exibirAlcanceSoldado(btn);
			}
			btnPrimeiroClique = btn;
		} else if (btn == btnPrimeiroClique) {
			// Clicou de novo no mesmo botão
			btnPrimeiroClique = null;
			btn.setSelected(false);
			limparAlcances();
			btn.setEnabled(true);
		} else if (isAindaNaoInteragiu && temSoldado && isAmigo) {
			// Clicou num aliado ainda não utilizado
			// Anula jogada atual e inicia uma nova
			btnPrimeiroClique.setSelected(false);
			btnPrimeiroClique = null;
			limparAlcances();
			cliqueCampo(btn);
		} else {
			// Segundo clique
			boolean isAlcance = btn.isAlcance();
			if (isAlcance || temSoldado && !isAmigo) {
				Posicao posicaoOrigem = btnPrimeiroClique.getPosicao();
				Posicao posicaoDestino = btn.getPosicao();
				if (temSoldado) {
					// Ataque
					Soldado soldadoAtacante = btnPrimeiroClique.getSoldadoOcupante();
					ControladorJogo.getInstance().enviaAtaque(soldadoAtacante, posicaoOrigem, posicaoDestino);
				} else {
					// Movimento
					ControladorJogo.getInstance().enviaMovimento(posicaoOrigem, posicaoDestino);
					Soldado soldadoMovimentado = btnPrimeiroClique.removerSoldado();
					btn.posicionarSoldado(soldadoMovimentado);
				}
				btn.setAindaNaoInteragiu(false);
				btnPrimeiroClique.setSelected(false);
				btnPrimeiroClique.setAindaNaoInteragiu(false);
				btnPrimeiroClique = null;
				exibeDadosSoldado(null);

				limparAlcances();

				ControladorJogo.getInstance().verificaPassaVez();
			}
			btn.setSelected(false);
		}
	}

	public void tratarJogada(JogadaEU jogada) {
		BotaoPosicao btn, btnOrigem = null, btnDestino = null;
		Posicao origem = jogada.getOrigem();
		Posicao destino = jogada.getDestino();

		for (Component c : painelCampoBatalha.getComponents()) {
			btn = (BotaoPosicao) c;
			if (btnOrigem == null && origem.equals(btn.getPosicao())) {
				btnOrigem = btn;
			} else if (btnDestino == null && destino.equals(btn.getPosicao())) {
				btnDestino = btn;
			}
			if (btnOrigem != null && btnDestino != null) {
				break;
			}
		}

		Soldado soldado;

		if (jogada instanceof Movimento) {
			soldado = btnOrigem.removerSoldado();
			btnDestino.posicionarSoldado(soldado);
		} else if (jogada instanceof Ataque) {
			Ataque a = (Ataque) jogada;
			soldado = btnDestino.getSoldadoOcupante();
			int dano = soldado.receberDano(a.getAtaqueMinimo(), a.getAtaqueMaximo());
			if (soldado.isVivo()) {
				ControladorJogo.getInstance().enviarEventoSoldadoFerido(dano);
			} else {
				ControladorJogo.getInstance().enviarEventoSoldadoAbatido(destino);
				btnDestino.removerSoldado();
				ControladorJogo.getInstance().verificaDerrota();
			}
			atualizarResumoTropa();
		}
	}

	public boolean isTodosSoldadosMortos() {
		return tropaAliada.isTodosSoldadosMortos();
	}
	
	public void removerSoldadoAbatido(Posicao posicaoSoldadoAbatido) {
		BotaoPosicao btn;
		for (Component c : painelCampoBatalha.getComponents()) {
			btn = (BotaoPosicao) c;
			if (posicaoSoldadoAbatido.equals(btn.getPosicao())) {
				btn.removerSoldado();
				painelCampoBatalha.invalidate();
				return;
			}
		}
	}

	private void atualizarResumoTropa() {
		List<Soldado> soldados = tropaAliada.getSoldados();
		Soldado s1 = soldados.get(0);
		Soldado s2 = soldados.get(1);
		Soldado s3 = soldados.get(2);
		Soldado s4 = soldados.get(3);
		Soldado s5 = soldados.get(4);

		mostraVida(lbVida1, s1);
		mostraVida(lbVida2, s2);
		mostraVida(lbVida3, s3);
		mostraVida(lbVida4, s4);
		mostraVida(lbVida5, s5);
	}

	private void limparAlcances() {
		Component[] botoes = painelCampoBatalha.getComponents();
		indicesAlcances.stream().forEach((i) -> {
			((BotaoPosicao) botoes[i]).exibirComoAlcance(false);
		});
		indicesAlcances.clear();
	}

	private void exibirAlcanceSoldado(BotaoPosicao btn) {
		Posicao p = btn.getPosicao();
		int alcance = btn.getSoldadoOcupante().getAlcance();
		int x = p.getX();
		int y = p.getY();

		pintaAlcance(x, y, alcance, true, true);
	}

	private void pintaAlcance(int x, int y, int alcance, boolean sobe, boolean desce) {
		if (alcance > -1) {
			Posicao p = new Posicao(x, y);
			Component[] components = painelCampoBatalha.getComponents();
			BotaoPosicao btn, btn1, btn2;

			for (int i = 0; i < components.length; i++) {
				btn = (BotaoPosicao) components[i];
				if (p.equals(btn.getPosicao())) {
					indicesAlcances.add(i);
					btn.exibirComoAlcance(true);
					int yPintar = p.getY();
					boolean atras = true;
					boolean frente = true;
					for (int j1 = i - 1, j2 = i + 1; i - j1 <= alcance; j1--, j2++) {
						try {
							if (atras) {
								btn1 = (BotaoPosicao) components[j1];
								if (yPintar == btn1.getPosicao().getY()) {
									btn1.exibirComoAlcance(true);
									indicesAlcances.add(j1);
								}
							}
						} catch (ArrayIndexOutOfBoundsException e) {
							atras = false;
						}

						try {
							if (frente) {
								btn2 = (BotaoPosicao) components[j2];
								if (yPintar == btn2.getPosicao().getY()) {
									btn2.exibirComoAlcance(true);
									indicesAlcances.add(j2);
								}
							}
						} catch (ArrayIndexOutOfBoundsException e) {
							frente = false;
						}
					}
					break;
				}
			}

			if (sobe) {
				pintaAlcance(x, y - 1, alcance - 1, true, false);
			}
			if (desce) {
				pintaAlcance(x, y + 1, alcance - 1, false, true);
			}
		}
	}

	private void exibeDadosSoldado(Soldado soldado) {
		if (soldado != null) {
			lbTipoAtual.setText(soldado.getTipo());
			mostraVida(lbVidaAtual, soldado);
			lbAlcanceAtual.setText(String.valueOf(soldado.getAlcance()));
			lbAtaqueAtual.setText(String.format("%d ~ %d", soldado.getAtaqueMinimo(), soldado.getAtaqueMaximo()));
			painelSoldadoAtual.setVisible(true);
		} else {
			painelSoldadoAtual.setVisible(false);
		}
	}

	private void mostraVida(JLabel label, Soldado soldado) {
		Color cor;
		int vida = soldado.getVida();
		if (vida >= 70) {
			cor = COR_VIDA_ALTA;
		} else if (vida >= 40) {
			cor = COR_VIDA_REGULAR;
		} else {
			cor = COR_VIDA_BAIXA;
		}
		label.setForeground(cor);
		label.setText(String.valueOf(vida));
	}
	
	public Batalha getBatalha() {
		return batalha;
	}

	/**
	 * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this
	 * method is always regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        painelCampoBatalha = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        lbOrdemJogador = new javax.swing.JLabel();
        painelSoldadoAtual = new javax.swing.JPanel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        lbTipoAtual = new javax.swing.JLabel();
        lbVidaAtual = new javax.swing.JLabel();
        lbAlcanceAtual = new javax.swing.JLabel();
        lbAtaqueAtual = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        lbVida1 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        lbVida2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        lbVida3 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        lbVida4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        lbVida5 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        lbVez = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                questionarAbandonarPartida(evt);
            }
        });

        painelCampoBatalha.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        painelCampoBatalha.setLayout(new java.awt.GridLayout(31, 31));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("Jogador");

        lbOrdemJogador.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lbOrdemJogador.setText("ordem");

        painelSoldadoAtual.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel22.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel22.setText("Soldado atual");

        jLabel23.setText("Tipo:");

        jLabel24.setText("Vida:");

        jLabel25.setText("Alcance:");

        jLabel26.setText("Ataque:");

        lbTipoAtual.setText("jLabel7");

        lbVidaAtual.setText("jLabel7");

        lbAlcanceAtual.setText("jLabel7");

        lbAtaqueAtual.setText("jLabel7");

        javax.swing.GroupLayout painelSoldadoAtualLayout = new javax.swing.GroupLayout(painelSoldadoAtual);
        painelSoldadoAtual.setLayout(painelSoldadoAtualLayout);
        painelSoldadoAtualLayout.setHorizontalGroup(
            painelSoldadoAtualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelSoldadoAtualLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(painelSoldadoAtualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel22)
                    .addGroup(painelSoldadoAtualLayout.createSequentialGroup()
                        .addComponent(jLabel23)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbTipoAtual))
                    .addGroup(painelSoldadoAtualLayout.createSequentialGroup()
                        .addComponent(jLabel24)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbVidaAtual))
                    .addGroup(painelSoldadoAtualLayout.createSequentialGroup()
                        .addComponent(jLabel25)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbAlcanceAtual))
                    .addGroup(painelSoldadoAtualLayout.createSequentialGroup()
                        .addComponent(jLabel26)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbAtaqueAtual)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        painelSoldadoAtualLayout.setVerticalGroup(
            painelSoldadoAtualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelSoldadoAtualLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel22)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(painelSoldadoAtualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel23)
                    .addComponent(lbTipoAtual))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(painelSoldadoAtualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel24)
                    .addComponent(lbVidaAtual))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(painelSoldadoAtualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel25)
                    .addComponent(lbAlcanceAtual))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(painelSoldadoAtualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel26)
                    .addComponent(lbAtaqueAtual))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        painelSoldadoAtual.setVisible(false);

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel2.setText("Soldado 1:");

        lbVida1.setForeground(new java.awt.Color(51, 102, 0));
        lbVida1.setText("100");

        jLabel8.setText("/");

        jLabel9.setText("100");

        jLabel12.setText("100");

        jLabel11.setText("/");

        lbVida2.setForeground(new java.awt.Color(51, 102, 0));
        lbVida2.setText("100");

        jLabel3.setText("Soldado 2:");

        jLabel4.setText("Soldado 3:");

        lbVida3.setForeground(new java.awt.Color(51, 102, 0));
        lbVida3.setText("100");

        jLabel14.setText("/");

        jLabel15.setText("100");

        jLabel18.setText("100");

        jLabel17.setText("/");

        lbVida4.setForeground(new java.awt.Color(51, 102, 0));
        lbVida4.setText("100");

        jLabel5.setText("Soldado 4:");

        jLabel6.setText("Soldado 5:");

        lbVida5.setForeground(new java.awt.Color(51, 102, 0));
        lbVida5.setText("100");

        jLabel20.setText("/");

        jLabel21.setText("100");

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel7.setText("Tropa");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbVida1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel9))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbVida2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel12))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbVida3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel14)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel15))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbVida4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel17)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel18))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbVida5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel20)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel21))
                    .addComponent(jLabel7))
                .addContainerGap(11, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(lbVida1)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(lbVida2)
                    .addComponent(jLabel11)
                    .addComponent(jLabel12))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(lbVida3)
                    .addComponent(jLabel14)
                    .addComponent(jLabel15))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(lbVida4)
                    .addComponent(jLabel17)
                    .addComponent(jLabel18))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(lbVida5)
                    .addComponent(jLabel20)
                    .addComponent(jLabel21))
                .addContainerGap())
        );

        lbVez.setText("jLabel10");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbOrdemJogador))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(painelSoldadoAtual, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(lbVez))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(painelCampoBatalha, javax.swing.GroupLayout.DEFAULT_SIZE, 507, Short.MAX_VALUE)
                .addGap(15, 15, 15))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(lbOrdemJogador))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbVez)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 50, Short.MAX_VALUE)
                        .addComponent(painelSoldadoAtual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(painelCampoBatalha, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void questionarAbandonarPartida(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_questionarAbandonarPartida
		AtorJogador.abandonarPartida();
    }//GEN-LAST:event_questionarAbandonarPartida

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lbAlcanceAtual;
    private javax.swing.JLabel lbAtaqueAtual;
    private javax.swing.JLabel lbOrdemJogador;
    private javax.swing.JLabel lbTipoAtual;
    private javax.swing.JLabel lbVez;
    private javax.swing.JLabel lbVida1;
    private javax.swing.JLabel lbVida2;
    private javax.swing.JLabel lbVida3;
    private javax.swing.JLabel lbVida4;
    private javax.swing.JLabel lbVida5;
    private javax.swing.JLabel lbVidaAtual;
    private javax.swing.JPanel painelCampoBatalha;
    private javax.swing.JPanel painelSoldadoAtual;
    // End of variables declaration//GEN-END:variables

}

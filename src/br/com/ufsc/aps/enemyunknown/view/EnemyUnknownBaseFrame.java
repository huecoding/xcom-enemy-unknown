/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ufsc.aps.enemyunknown.view;

import br.com.ufsc.aps.enemyunknown.network.AtorNetGames;
import java.awt.HeadlessException;
import javax.swing.JFrame;

/**
 *
 * @author Raphael
 */
public class EnemyUnknownBaseFrame extends JFrame {

    public EnemyUnknownBaseFrame(String title) throws HeadlessException {
        super(String.format("%s - %s", AtorNetGames.getInstance().getJogadorAtual().getNome(), title));
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ufsc.aps.enemyunknown.view;

import br.com.ufsc.aps.enemyunknown.model.SoldadoArmadura;
import br.com.ufsc.aps.enemyunknown.model.SoldadoForca;
import br.com.ufsc.aps.enemyunknown.model.SoldadoRegular;
import br.com.ufsc.aps.enemyunknown.model.Tropa;
import br.com.ufsc.aps.enemyunknown.util.AtorJogador;
import java.util.Arrays;

/**
 *
 * @author Raphael
 */
public class TelaEscolherTropa extends EnemyUnknownBaseFrame {

    public static final Tropa ATAQUE_FORTE = new Tropa("Ataque forte",
            Arrays.asList(
                    new SoldadoForca(1),
                    new SoldadoForca(2),
                    new SoldadoForca(3),
                    new SoldadoForca(4),
                    new SoldadoForca(5)
            )
    );

    public static final Tropa ATAQUE_MODERADO = new Tropa("Ataque moderado",
            Arrays.asList(
                    new SoldadoForca(1),
                    new SoldadoForca(2),
                    new SoldadoForca(3),
                    new SoldadoArmadura(4),
                    new SoldadoRegular(5)
            )
    );

    public static final Tropa INTERMEDIARIO = new Tropa("Intermediário",
            Arrays.asList(
                    new SoldadoForca(1),
                    new SoldadoArmadura(2),
                    new SoldadoRegular(3),
                    new SoldadoRegular(4),
                    new SoldadoRegular(5)
            )
    );

    public static final Tropa DEFESA_MODERADA = new Tropa("Defesa moderada",
            Arrays.asList(
                    new SoldadoArmadura(1),
                    new SoldadoArmadura(2),
                    new SoldadoArmadura(3),
                    new SoldadoForca(4),
                    new SoldadoRegular(5)
            )
    );

    public static final Tropa DEFESA_FORTE = new Tropa("Defesa forte",
            Arrays.asList(
                    new SoldadoArmadura(1),
                    new SoldadoArmadura(2),
                    new SoldadoArmadura(3),
                    new SoldadoArmadura(4),
                    new SoldadoArmadura(5)
            )
    );

    /**
     * Creates new form TelaEscolherTropa
     */
    public TelaEscolherTropa() {
        super("Escolher tropa");
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        group = new javax.swing.ButtonGroup();
        rbAtaqueForte = new javax.swing.JRadioButton();
        rbAtaqueModerado = new javax.swing.JRadioButton();
        rbIntermediario = new javax.swing.JRadioButton();
        rbDefesaModerada = new javax.swing.JRadioButton();
        rbDefesaForte = new javax.swing.JRadioButton();
        OK = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);

        group.add(rbAtaqueForte);
        rbAtaqueForte.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        rbAtaqueForte.setText("Ataque forte");
        rbAtaqueForte.setActionCommand("1");

        group.add(rbAtaqueModerado);
        rbAtaqueModerado.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        rbAtaqueModerado.setText("Ataque moderado");
        rbAtaqueModerado.setActionCommand("2");

        group.add(rbIntermediario);
        rbIntermediario.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        rbIntermediario.setSelected(true);
        rbIntermediario.setText("Intermediário");
        rbIntermediario.setActionCommand("3");

        group.add(rbDefesaModerada);
        rbDefesaModerada.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        rbDefesaModerada.setText("Defesa moderada");
        rbDefesaModerada.setActionCommand("4");

        group.add(rbDefesaForte);
        rbDefesaForte.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        rbDefesaForte.setText("Defesa forte");
        rbDefesaForte.setActionCommand("5");

        OK.setText("Ready!");
        OK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                escolherTropa(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(OK, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(rbAtaqueForte)
                            .addComponent(rbAtaqueModerado)
                            .addComponent(rbIntermediario)
                            .addComponent(rbDefesaModerada)
                            .addComponent(rbDefesaForte))
                        .addGap(0, 175, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(rbAtaqueForte)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rbAtaqueModerado)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rbIntermediario)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rbDefesaModerada)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rbDefesaForte)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(OK, javax.swing.GroupLayout.DEFAULT_SIZE, 36, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void escolherTropa(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_escolherTropa
        Tropa t;
		String selecao = group.getSelection().getActionCommand();
        switch (selecao) {
            case "1":
                t = ATAQUE_FORTE;
                break;

            case "2":
                t = ATAQUE_MODERADO;
                break;

            case "3":
                t = INTERMEDIARIO;
                break;

            case "4":
                t = DEFESA_MODERADA;
                break;

            case "5":
                t = DEFESA_FORTE;
                break;

            default:
                t = null;
                break;
        }
        dispose();
        AtorJogador.notificarTropaEscolhida(t);
    }//GEN-LAST:event_escolherTropa

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton OK;
    private javax.swing.ButtonGroup group;
    private javax.swing.JRadioButton rbAtaqueForte;
    private javax.swing.JRadioButton rbAtaqueModerado;
    private javax.swing.JRadioButton rbDefesaForte;
    private javax.swing.JRadioButton rbDefesaModerada;
    private javax.swing.JRadioButton rbIntermediario;
    // End of variables declaration//GEN-END:variables
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ufsc.aps.enemyunknown.util;

import br.com.ufsc.aps.enemyunknown.EnemyUnknown;
import br.com.ufsc.aps.enemyunknown.exception.FaltamJogadoresException;
import br.com.ufsc.aps.enemyunknown.model.Batalha;
import br.com.ufsc.aps.enemyunknown.model.Tropa;
import br.com.ufsc.aps.enemyunknown.network.AtorNetGames;
import br.com.ufsc.aps.enemyunknown.network.util.TipoMensagem;
import br.com.ufsc.aps.enemyunknown.view.BotaoPosicao;
import br.com.ufsc.aps.enemyunknown.view.CampoBatalha;
import br.com.ufsc.aps.enemyunknown.view.TelaAguardandoOutroJogador;
import br.com.ufsc.aps.enemyunknown.view.TelaEscolherTropa;
import br.ufsc.inf.leobr.cliente.exception.NaoConectadoException;
import java.awt.EventQueue;
import java.text.SimpleDateFormat;
import javax.swing.JOptionPane;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author raphael
 */
public class AtorJogador {

	public static String pedirNome() {
		String nome = JOptionPane.showInputDialog("Novo nome do jogador");
		if (nome == null || StringUtils.isNotBlank(nome)) {
			return nome;
		}
		return pedirNome();
	}

	public static String pedirEnderecoServidor() {
		String endereco = JOptionPane.showInputDialog("Endereço do servidor");
		if (endereco == null || StringUtils.isNotBlank(endereco)) {
			return endereco;
		}
		return pedirEnderecoServidor();
	}

	public static void exibirDadosServidor() {
		showMessage(String.format("Conectado a %s", AtorNetGames.getInstance().getEndereco()), "Conectado", JOptionPane.INFORMATION_MESSAGE);
	}

	public static boolean conectar(String enderecoServidor) {
		try {
			AtorNetGames.getInstance().tentaConectar(enderecoServidor);
			return true;
		} catch (Exception ex) {
			notificarFalhaConexao(ex);
			return false;
		}
	}

	private static void notificarFalhaConexao(Exception falha) {
		showMessage(falha.getMessage(), "Falha de conexão", JOptionPane.ERROR_MESSAGE);
	}

	public static void propoeInicioPartida() {
		try {
			AtorNetGames.getInstance().iniciarPartida();
		} catch (NaoConectadoException | FaltamJogadoresException ex) {
			AtorJogador.exibirFalhaGenerica(ex);
		}
	}

	public static boolean notificarSolicitacaoInicio(String jogadorSolicitante) {
		return JOptionPane.showConfirmDialog(
			null,
			String.format("O jogador %s solicitou início de partida. Deseja aceitar?", jogadorSolicitante),
			"Solicitação de início de partida",
			JOptionPane.YES_NO_OPTION,
			JOptionPane.QUESTION_MESSAGE
		) == JOptionPane.YES_OPTION;
	}

	public static void setAguardando(boolean aguardando) {
		TelaAguardandoOutroJogador.getInstance().setVisible(aguardando);
	}

	public static void exibirJogadorRecusouInicio() {
		showMessage("Jogador recusou início de partida.", "Recusou início", JOptionPane.WARNING_MESSAGE);
	}

	public static void escolherTropa() {
		new TelaEscolherTropa().setVisible(true);
	}

	public static void notificarTropaEscolhida(Tropa tropa) {
		AtorNetGames.getInstance().getJogadorAtual().setTropa(tropa);
		setAguardando(true);
		AtorNetGames.getInstance().enviarMensagem(TipoMensagem.TROPA_ESCOLHIDA);
	}

	public static void exibirCampoDeBatalha(Batalha batalha) {
		EnemyUnknown.getTelaInicial().setVisible(false);
		CampoBatalha.getInstance().iniciarBatalha(batalha);
	}

	public static void exibirDanoCausado(int dano) {
		showMessage(String.format("Dano causado: %d", dano), "Soldado ferido", JOptionPane.INFORMATION_MESSAGE);
	}

	public static void exibirSoldadoAbatido() {
		showMessage("Parabéns! Você abateu um soldado inimigo!", "Soldado abatido", JOptionPane.INFORMATION_MESSAGE);
	}

	public static void abandonarPartida() {
		if (confirmaAbandonarPartida()) {
			AtorNetGames.getInstance().enviarMensagem(TipoMensagem.ABANDONAR_PARTIDA);
			CampoBatalha.tratarFimBatalha();
			EnemyUnknown.getTelaInicial().setVisible(true);
		}
	}

	private static boolean confirmaAbandonarPartida() {
		return JOptionPane.showConfirmDialog(
			null,
			"Tem certeza de que deseja abandonar a batalha?",
			"Abandonar partida",
			JOptionPane.YES_NO_OPTION,
			JOptionPane.YES_OPTION
		) == JOptionPane.YES_OPTION;
	}

	public static void exibirJogadorAbandonouPartida(String nomeJogador) {
		showMessage(
			String.format("%s abandonou a partida! Fechando campo de batalha...", nomeJogador),
			"Fim da batalha",
			JOptionPane.INFORMATION_MESSAGE
		);
	}

	public static boolean desconectar() {
		if (confirmaDesconexao()) {
			try {
				AtorNetGames.getInstance().desconectar();
				return true;
			} catch (NaoConectadoException ex) {
				exibirFalhaDesconexao(ex);
				return false;
			}
		}
		return false;
	}

	private static boolean confirmaDesconexao() {
		return JOptionPane.showConfirmDialog(null,
			String.format("Tem certeza de que deseja desconectar de %s?", AtorNetGames.getInstance().getEndereco()),
			"Desconectar",
			JOptionPane.YES_NO_OPTION,
			JOptionPane.YES_OPTION
		) == JOptionPane.YES_OPTION;
	}

	public static void exibirFalhaDesconexao(Exception falha) {
		exibirFalhaGenerica(falha);
	}

	public static void exibirResumo(Batalha batalha) {
		String msg, title;
		if (batalha.getOrdemVencedor() == AtorNetGames.getInstance().getJogadorAtual().getOrdem()) {
			msg = "Fim de batalha! Você venceu!\nDuração da partida: %s";
			title = "Vitória";
		} else {
			msg = "Fim de batalha! Você perdeu!\nDuração da partida: %s";
			title = "Derrota";
		}
		showMessage(String.format(msg, new SimpleDateFormat("mm:ss").format(batalha.getDuracao())), title, JOptionPane.INFORMATION_MESSAGE);
	}

	public static void exibirFalhaGenerica(Exception falha) {
		showMessage(falha.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
	}
	
	public static void trataCliqueCampo(BotaoPosicao btn) {
		CampoBatalha.getInstance().cliqueCampo(btn);
	}
	
	public static void setBloqueioBotoes(boolean bloqueia){
		EnemyUnknown.getTelaInicial().setBloqueioBotoes(bloqueia);
	}

	private static void showMessage(String message, String title, int type) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				JOptionPane.showMessageDialog(null, message, title, type);
			}
		});
	}

}

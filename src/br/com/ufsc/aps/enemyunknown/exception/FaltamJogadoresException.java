/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ufsc.aps.enemyunknown.exception;

/**
 *
 * @author Raphael
 */
public class FaltamJogadoresException extends Exception{

    public FaltamJogadoresException() {
        super("O número de jogadores é insufisciente para iniciar uma partida!");
    }
    
}

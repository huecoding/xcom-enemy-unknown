/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ufsc.aps.enemyunknown.exception;

/**
 *
 * @author Raphael
 */
public class NomeJaExisteNoServidorException extends Exception {

	public NomeJaExisteNoServidorException() {
		super("Um jogador com este nome já está conectado ao servidor!");
	}

}

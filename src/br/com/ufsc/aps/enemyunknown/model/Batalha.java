/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ufsc.aps.enemyunknown.model;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author raphael
 */
public class Batalha implements Serializable {

	private Date inicio;
	private Date fim;
	private int ordemVencedor;

	public void iniciar() {
		inicio = new Date();
	}

	public void finalizar(int ordemVencedor) {
		setOrdemVencedor(ordemVencedor);
		fim = new Date();
	}

	public Date getDuracao() {
		return new Date(fim.getTime() - inicio.getTime());
	}

	public int getOrdemVencedor() {
		return ordemVencedor;
	}

	public void setOrdemVencedor(int ordemVencedor) {
		this.ordemVencedor = ordemVencedor;
	}

}

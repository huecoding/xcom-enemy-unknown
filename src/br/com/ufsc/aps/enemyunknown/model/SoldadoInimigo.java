/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ufsc.aps.enemyunknown.model;

/**
 *
 * @author Raphael
 */
public class SoldadoInimigo extends Soldado {

    public SoldadoInimigo() {
        super(0, 0, 0, 0, 0);
    }

    @Override
    public boolean isAmigo() {
        return false;
    }

}

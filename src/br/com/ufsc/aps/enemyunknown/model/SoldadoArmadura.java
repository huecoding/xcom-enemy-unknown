/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ufsc.aps.enemyunknown.model;

/**
 *
 * @author raphael
 */
public class SoldadoArmadura extends Soldado {

    private static final int VIDA = 150;
    private static final int ALCANCE = 4;
    private static final int ATAQUE_MAXIMO = 40;
    private static final int ATAQUE_MINIMO = 20;

    public SoldadoArmadura(int ordem) {
        super(VIDA, ALCANCE, ATAQUE_MAXIMO, ATAQUE_MINIMO, ordem);
    }

}

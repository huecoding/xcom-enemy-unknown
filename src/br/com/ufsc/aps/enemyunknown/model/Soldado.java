/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ufsc.aps.enemyunknown.model;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author raphael
 */
public abstract class Soldado {

	private int vida;

	private final int alcance;
	private final int ordem;
	private final int ataqueMaximo;
	private final int ataqueMinimo;

	protected Soldado(int vida, int alcance, int ataqueMaximo, int ataqueMinimo, int ordem) {
		this.vida = vida;
		this.alcance = alcance;
		this.ataqueMaximo = ataqueMaximo;
		this.ataqueMinimo = ataqueMinimo;
		this.ordem = ordem;
	}
	
	public String getTipo() {
		return getClass().getSimpleName().replace("Soldado", StringUtils.EMPTY);
	}

	public boolean isAmigo() {
		return true;
	}

	public int receberDano(int ataqueMinimo, int ataqueMaximo) {
		int danoCausado = ataqueMinimo + (int) (Math.random() * ((ataqueMaximo - ataqueMinimo) + 1));
		vida -= danoCausado;
		return danoCausado;
	}

	public boolean isVivo() {
		return vida > 0;
	}

	public int getVida() {
		return vida > 0 ? vida : 0;
	}

	public int getAlcance() {
		return alcance;
	}

	public int getOrdem() {
		return ordem;
	}

	public int getAtaqueMaximo() {
		return ataqueMaximo;
	}

	public int getAtaqueMinimo() {
		return ataqueMinimo;
	}

}

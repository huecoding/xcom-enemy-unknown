/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ufsc.aps.enemyunknown.model;

import java.util.List;

/**
 *
 * @author raphael
 */
public class Tropa {

	private String nome;
	private List<Soldado> soldados;

	public Tropa(String nome, List<Soldado> soldados) {
		this.nome = nome;
		this.soldados = soldados;
	}

	public boolean isTodosSoldadosMortos() {
		for (Soldado soldado : soldados) {
			if (soldado.isVivo()) {
				return false;
			}
		}
		return true;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Soldado> getSoldados() {
		return soldados;
	}

	public void setSoldados(List<Soldado> soldados) {
		this.soldados = soldados;
	}

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ufsc.aps.enemyunknown.model;

/**
 *
 * @author Raphael
 */
public class JogadorPessoa extends Jogador {

	private Tropa tropa;

	public JogadorPessoa() {
		super();
	}
	
	public Tropa getTropa() {
		return tropa;
	}

	public void setTropa(Tropa tropa) {
		this.tropa = tropa;
	}
}

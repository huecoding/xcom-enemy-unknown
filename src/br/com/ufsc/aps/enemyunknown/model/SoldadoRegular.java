/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ufsc.aps.enemyunknown.model;

/**
 *
 * @author raphael
 */
public class SoldadoRegular extends Soldado {

    private static final int VIDA = 100;
    private static final int ALCANCE = 5;
    private static final int ATAQUE_MAXIMO = 50;
    private static final int ATAQUE_MINIMO = 30;

    public SoldadoRegular(int ordem) {
        super(VIDA, ALCANCE, ATAQUE_MAXIMO, ATAQUE_MINIMO, ordem);
    }

}
